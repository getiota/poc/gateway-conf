# Istio Gateway Conf

Another way to easily generate the configuration for a simple Istio Gateway with JWT auth.

## Goals

With the raw configuration, you need to have 4 differents workloads:
- a `Gateway` workload that defines the gateway itself
- a `VirtualService` workload that defines the route
- an `AuthorizationPolicy` workload that defines the requirements for each route
- a `RequestAuthentication` workload that defines which JWT signatures are allowed

The connection betwen the files is rather verbose and error prone. For example, if you want the path `/hello` to
require a specific JWT claim you would need to define that path twice: once in the service and once in the policy.

The goal of this program is to make it easier and safer to generate that configuration for a setup where each route
to the gateway is protected by a JWT auth.

### Config File

An example of a config file is available [here](fixtures/gateway.yml)

## Setup

This is a python script. Best way is to use a virtual environment:
```sh
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python generate.py /path/to/conf
```