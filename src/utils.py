import argparse
import json
import os

import yaml
from jwcrypto import jwk


def pem_to_jwks(pem: str) -> str:
    key = jwk.JWK.from_pem(pem.encode())
    key = key.export_public()

    key = {'keys': [key]}

    return json.dumps(key)


def map_jwt_rule(key: dict, forwardTo: str) -> dict:
    # Replace PEM if possible
    try:
        p = key.pop('pem')
        key['jwks'] = pem_to_jwks(p)
    except KeyError:
        pass

    if forwardTo:
        key['outputPayloadToHeader'] = forwardTo
    return key


def jwt_rules(keys: list, forwardTo: str) -> list:
    return [map_jwt_rule(k, forwardTo) for k in keys]


def merge_payloads(payloads: list) -> str:
    """Merges payloads in a single yaml file"""
    return '---\n'.join((yaml.dump(p) for p in payloads))


def argument_parser():
    parser = argparse.ArgumentParser(description='Generate Istio Gateway conf')
    parser.add_argument(
        'conf',
        type=lambda x: is_valid_yaml(parser, x),
        help="The configuration file path"
    )
    return parser


def is_valid_yaml(parser: argparse.ArgumentParser, arg: str):
    if not os.path.exists(arg):
        parser.error(f'The file {arg} does not exist!')
    with open(arg, 'r') as file:
        content = file.read()

    try:
        return yaml.load(content, Loader=yaml.SafeLoader)
    except yaml.YAMLError as e:
        parser.error(f'Invalid YAML:\n{e}')


def app_to_http(app: dict) -> dict:
    paths = []
    for r in app['routes']:
        for to in r['to']:
            paths += to['paths']

    return {
        'match': [{'uri': {'exact': p}} for p in paths],
        'route': [{
            'destination': {
                'host': app['name'],
                'port': {
                    'number': app['port']
                }
            }
        }]
    }


def route_to_rule(route: dict) -> dict:
    return {
        **route.get('auth', {}),
        'to': [
            {
                'operation': {
                    'methods': to.get('methods', ['GET']),
                    'paths': to['paths']
                }
            }
            for to in route['to']
        ]
    }


def app_to_rules(app: dict) -> list:
    return [route_to_rule(r) for r in app['routes']]
