#!/usr/bin/python

import payloads
import utils


def request_authentication(name: str, keys: list, forwardTo: str) -> dict:
    return payloads.authentication_payload(
        name,
        utils.jwt_rules(keys, forwardTo)
    )


def parse_hosts(apps) -> (list, list):
    """Returns http and rules"""
    https = []
    rules = []

    for a in apps:
        https.append(utils.app_to_http(a))
        rules += utils.app_to_rules(a)

    return https, rules


if __name__ == "__main__":
    parser = utils.argument_parser()
    args = parser.parse_args()
    conf = args.conf

    try:
        name = conf['name']
    except KeyError:
        raise SystemExit('Missing `name` in configuration')

    workloads = [payloads.gateway_payload(name, conf.get('servers'))]

    keys = conf.get('keys')
    if keys:
        workloads.append(
            request_authentication(name, keys, conf.get('forwardJWTHeader'))
        )

    apps = conf.get('apps')
    if apps:
        http, rules = parse_hosts(apps)
        workloads.append(payloads.service_payload(name, conf.get('hosts'), http))
        workloads.append(payloads.authorization_payload(name, rules))

    with open('./out.yaml', 'w') as file:
        file.write(utils.merge_payloads(workloads))
