ISTIO_SYSTEM = 'istio-system'
GATEWAY_SELECTOR = {
    'matchLabels': {
        'istio': 'ingressgateway'
    }
}


def base_payload(kind: str, version: str, name: str, spec: dict, namespace='default'):
    return {
        'apiVersion': version,
        'kind': kind,
        'metadata': {
            'name': name,
            'namespace': namespace
        },
        'spec': spec,
    }


def gateway_payload(name: str, servers: dict):
    return base_payload(
        kind="Gateway",
        version="networking.istio.io/v1beta1",
        name=name,
        spec={
            'selector': GATEWAY_SELECTOR['matchLabels'],
            'servers': servers,
        }
    )


def service_payload(gateway: str, hosts: list, http: list):
    return base_payload(
        kind='VirtualService',
        version='networking.istio.io/v1beta1',
        name=gateway + '-service',
        spec={
            'hosts': hosts,
            'gateways': [gateway],
            'http': http,
        }
    )


def authorization_payload(gateway: str, rules: list):
    return base_payload(
        kind='AuthorizationPolicy',
        version='security.istio.io/v1beta1',
        name=gateway + '-auth',
        namespace=ISTIO_SYSTEM,
        spec={
            'action': 'ALLOW',
            'selector': GATEWAY_SELECTOR,
            'rules': rules,
        }
    )


def authentication_payload(gateway: str, jwt_rules: list):
    return base_payload(
        kind='RequestAuthentication',
        version='security.istio.io/v1beta1',
        name=gateway + '-jwts',
        namespace=ISTIO_SYSTEM,
        spec={
            'selector': GATEWAY_SELECTOR,
            'jwtRules': jwt_rules
        }
    )
