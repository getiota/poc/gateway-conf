import json
from pathlib import Path

import yaml

import utils

FIXTURES = Path(__file__).parent.parent / 'fixtures'


def test_pem_to_jwks():
    with open(FIXTURES / 'pub.pem', 'r') as file:
        pem = file.read()

    out = json.loads(utils.pem_to_jwks(pem))
    assert len(out['keys']) == 1


def test_merge_payloads():
    out = utils.merge_payloads([{'hello': ['h']}, ['me', 'you'], {'route': 'hello'}])
    assert len(out.split('---\n')) == 3


def test_route_to_rule():
    app = yaml.load("""
to:
- paths: ["/transfer"]
  methods: ["POST"]
auth:
  when:
  - key: request.auth.claims[roles]
    values: ["role2"]
    """, Loader=yaml.SafeLoader)

    rule = utils.route_to_rule(app)
    assert rule == {
        'to': [
            {
                'operation': {
                    'paths': ['/transfer'],
                    'methods': ['POST'],
                },
            }
        ],
        'when': [
            {
                'key': 'request.auth.claims[roles]',
                'values': ['role2'],
            }
        ]
    }
